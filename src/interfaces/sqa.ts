export interface Sqa {
  attributeAnswer1: string;
  attributeAnswer2: string;
  attributeQuestion1: string;
  attributeQuestion2: string;
  secondaryEmail: string;
}
