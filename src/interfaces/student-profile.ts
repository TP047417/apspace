export interface StudentProfile {
  STUDENT_NUMBER: string;
  COUNTRY: string;
  NAME: string;
  INTAKE: string;
  PROGRAMME: string;
  MENTOR_SAMACCOUNTNAME: string;
  STUDENT_STATUS: string;
  STUDENT_EMAIL: string;
  INTAKE_STATUS: string;
  PHOTO_NO: any;
  PL_NAME: string;
  PL_SAMACCOUNTNAME: string;
  MENTOR_NAME: string;
  PROVIDER_CODE: string;
  BLOCK: boolean;
  MESSAGE: string;
  EMGS_COUNTRY_CODE: string;
  IC_PASSPORT_NO: string;
}
