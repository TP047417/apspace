export interface AplcStudentBehaviour {
  STUDENT_NUMBER: string;
  IC_PASSPORT: string;
  
  AVERAGE_BEH: number;
  ACADEMIC_BEH: number;
  SOCIAL_BEH: number;
  COMPLETING_BEH: number;
  CONCEPT_BEH: number;
  REMARK: string;
  
  CLASS_CODE: string;
  SUBJECT_CODE: string;
  DATE_UPDATED: string;
  LECTURER_CODE: string;
  LECTURER_UPDATED: string;
}
