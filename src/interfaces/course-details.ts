export interface CourseDetails {
  SEMESTER: number;
  TOTAL_MODULES_PASSED: number;
  TOTAL_CREDIT_HOURS: number;
  CREDIT_TRANSFERRED: number;
  GPA: number;
  SEMESTER_ATTENDANCE: number;
  IMMIGRATION_GPA: number;
}
