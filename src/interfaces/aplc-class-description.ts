export interface AplcClassDescription {
  CLASS_DESCRIPTION: string,
  LECTURER_NAME: string,
  EDATE: string;
  SDATE: string,
}
