import { NgModule } from '@angular/core';
import { AutosizeDirective } from './autoSize/auto-size';

@NgModule({
  declarations: [AutosizeDirective],
  imports: [],
  exports: [AutosizeDirective],
})
export class DirectivesModule { }
