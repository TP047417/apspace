import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UnavailabledetailsPage } from './unavailabledetails';

@NgModule({
  declarations: [
    UnavailabledetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(UnavailabledetailsPage),
  ],
})
export class UnavailabledetailsPageModule { }
